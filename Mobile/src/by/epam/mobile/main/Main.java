package by.epam.mobile.main;

import java.util.Set;

import org.apache.log4j.Logger;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.exception.MoblieException;
import by.epam.mobile.report.Reporter;
import by.epam.mobile.service.CalculationService;
import by.epam.mobile.service.ParseService;
import by.epam.mobile.service.SearchService;
import by.epam.mobile.service.SearchSortParameter;
import by.epam.mobile.service.SortService;
import by.epam.mobile.service.comparator.TariffComparator;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class);
	
	private static final String XML_FILE_PATH = "xml/tariffs.xml";
	private static final SearchSortParameter SORT_PARAMETER = SearchSortParameter.CONNECTION_FEE;
	private static final SearchSortParameter SEARCH_PARAMETER = SearchSortParameter.CONNECTION_FEE;
	private static final int SEARCH_RANGE_FLOOR = 30_000;
	private static final int SEARCH_RANGE_CELL = 110_000;
	
	public static void main(String[] args) {
		LOG.info("Program has been started!\n");
		
		TariffSet tariffSet = new TariffSet();
		Set<Tariff> sortedSet = null, resultingSet = null;
		long sum = 0L;
		
		// Парсинг xml-документа с данными 
		try {
			ParseService.parseDocument(tariffSet, XML_FILE_PATH);
			Reporter.displayTextTariffSet();
			Reporter.displayTariffSet(tariffSet.getTariffSet());
		} catch (MoblieException ex) {
			LOG.error("Some problems occured in parsing process\n" + ex.getMessage());
		}
		
		// Сортировка тарифов в наборе по размеру абонентской платы
		sortedSet = SortService.sortByComparator(tariffSet, new TariffComparator(SORT_PARAMETER));
		Reporter.displayTextAfterSorting(SORT_PARAMETER);
		Reporter.displayTariffSet(sortedSet);
		
		// Расчёт суммы абонентской платы всех тарифов из набора
		try {
			sum = CalculationService.sumConnectionFee(tariffSet);
			Reporter.displayTextAfterSumming();
			Reporter.displayNumericParameter(sum);
		} catch (MoblieException ex) {
			LOG.error("Incorrect data for counting sum! " + ex.getMessage());
		}
		
		// Поиск тарифов в наборе, чья абонентская плата попадает в диапазон, заданный константами
		LOG.info("Searching process has started. range floor=" + SEARCH_RANGE_FLOOR + " range cell=" + SEARCH_RANGE_CELL + "\n");
		try {
			 resultingSet = SearchService.searchTariffByRange(tariffSet, SEARCH_PARAMETER, SEARCH_RANGE_FLOOR, SEARCH_RANGE_CELL);
		} catch (MoblieException ex) {
			LOG.error("Wrong search parameter... " + ex.getMessage());
		}
		if(!resultingSet.isEmpty()) {
			Reporter.displayTextAfterPositiveSearching(SEARCH_RANGE_FLOOR, SEARCH_RANGE_CELL);
			Reporter.displayTariffSet(resultingSet);
			LOG.info(resultingSet.size() + " tariffs were found.\n");
		} else {
			Reporter.displayTextAfterNegativeSearching(SEARCH_RANGE_FLOOR, SEARCH_RANGE_CELL);
			LOG.error("No tariffs were found...");
		}
				
	}

}
