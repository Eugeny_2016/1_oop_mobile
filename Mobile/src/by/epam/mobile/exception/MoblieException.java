package by.epam.mobile.exception;

public class MoblieException extends Exception {

	private static final long serialVersionUID = 1L;

	public MoblieException() {
		super();
	}
	
	public MoblieException(String msg) {
		super(msg);
	}
	
	public MoblieException(Throwable ex) {
		super(ex);
	}
	
	public MoblieException(String msg, Throwable ex) {
		super(msg, ex);
	}

}
