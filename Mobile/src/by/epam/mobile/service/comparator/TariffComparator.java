package by.epam.mobile.service.comparator;

import java.util.Comparator;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.service.CallType;
import by.epam.mobile.service.SearchSortParameter;

// универсальный компаратор, сравнивающий исходя из переданного параметра сортировки (и типа звонка, если нужно)
// по аналогии с учебником стр 265
public class TariffComparator implements Comparator<Tariff> {
	
	private SearchSortParameter sortingParameter;
	private CallType callType;	// сюда передаётся значение извне, только если сортировка осуществляется по параметру, встречающемуся у нескольких типов звонков  
	
	public TariffComparator(SearchSortParameter sortingParameter) {
		setSortingParameter(sortingParameter);
	}
	
	public TariffComparator(SearchSortParameter sortingParameter, CallType callType) {
		setSortingParameter(sortingParameter);
		setCallType(callType);
	}
	
	public final void setSortingParameter(SearchSortParameter sortingParameter) {
		if (sortingParameter == null) {
			throw new IllegalArgumentException();
		}
		this.sortingParameter = sortingParameter;
	}
	
	public final void setCallType(CallType callType) {
		if (callType == null) {
			throw new IllegalArgumentException();
		}
		this.callType = callType;	
	}
	
	public SearchSortParameter getSortingParameter() {
		return sortingParameter;
	}

	public CallType getCallType() {
		return callType;
	}

	@Override
	public int compare(Tariff tariff1, Tariff tariff2) {
		int result = 0;
		
		switch (sortingParameter) {
		case CONNECTION_FEE:
			result = tariff1.getConnectionFee() - tariff2.getConnectionFee();
			break;
		case TARIFF_NAME:
			result =  tariff1.getTariffName().compareTo(tariff2.getTariffName());  // String реализует Comparable, поэтому переопределённый в нём метод compareTo() сам вернёт коллекции полож/отриц/0 число
			break;
		case COMMON_MINUTE_PRICE:	// этот параметр есть у нескольких типов звонков, поэтому ещё один switch
			switch (callType) {
			case NATIVE:
				result = (Integer)tariff1.getNativePriceTable().get(sortingParameter) - (Integer)tariff2.getNativePriceTable().get(sortingParameter);
				break;
			case STATIONARY:
				result = (Integer)tariff1.getStationaryPriceTable().get(sortingParameter) - (Integer)tariff2.getStationaryPriceTable().get(sortingParameter);
				break;
			case OTHERNET:
				result = (Integer)tariff1.getOtherNetworkPriceTable().get(sortingParameter) - (Integer)tariff2.getOtherNetworkPriceTable().get(sortingParameter);
				break;
			}
			break;
		default:
			throw new UnsupportedOperationException();
			// после добавления в switch всех параметров сортировки из перечисления
			// это исключение будет заменено на EnumConstantNotPresentException(SearchSortParameter.class, sortingParameter.name());
		}		
		return result;
	}
}
