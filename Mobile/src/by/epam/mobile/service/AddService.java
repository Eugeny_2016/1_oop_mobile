package by.epam.mobile.service;

import java.util.Map;

import by.epam.mobile.entity.CallParameterName;
import by.epam.mobile.entity.CallTariff;
import by.epam.mobile.entity.InternetParameterName;
import by.epam.mobile.entity.InternetTariff;
import by.epam.mobile.entity.Tariff;
import by.epam.mobile.entity.TariffName;
import by.epam.mobile.entity.TariffSet;

public class AddService {
	// вставка тарифа для звонков
	public static boolean addTariff(TariffSet tariffSet, int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice,int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		Tariff tariff = new CallTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment,
									   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		
		return tariffSet.addToTariffSet(tariff);
	}
	
	// вставка тарифа для интернета
	public static boolean addTariff(TariffSet tariffSet, int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
								   int internetSpeedDefault, int internetSpeedMax, int smsPrice, int mmsPrice,
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable, 
								   Map<InternetParameterName, Object> internetPriceTable) {
		
		Tariff tariff = new InternetTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
										   internetSpeedDefault, internetSpeedMax, smsPrice, mmsPrice,
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable,
										   internetPriceTable);

		return tariffSet.addToTariffSet(tariff);
	}
}
