package by.epam.mobile.service;

import java.util.Set;
import java.util.TreeSet;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.exception.MoblieException;
import by.epam.mobile.service.comparator.TariffComparator;

public class SearchService {
		
	public static Set<Tariff> searchTariffByRange(TariffSet tariffSet, SearchSortParameter searchParameter, int searchRangeFloor, int searchRangeCell) throws MoblieException {
		Set<Tariff> resultingSet = new TreeSet<Tariff>(new TariffComparator(searchParameter));
		switch (searchParameter) {
		case CONNECTION_FEE:
			for (Tariff tariff : tariffSet.getTariffSet()) {
				if ((tariff.getConnectionFee() >= searchRangeFloor) && (tariff.getConnectionFee() <= searchRangeCell)) {
					resultingSet.add(tariff);
				}
			}
			break;
		// ещё можно добавить case для других числовых параметров тарифа
		// default-case здесь нужен для случаев, когда был передан поисковый параметр строкового типа 
		default:
			throw new MoblieException("Can't find anything with such parameter!");
		}
		return resultingSet;
	}
}
