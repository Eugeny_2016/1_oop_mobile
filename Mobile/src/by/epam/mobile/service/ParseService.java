package by.epam.mobile.service;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.exception.MoblieException;
import by.epam.mobile.xml.tariff.sax.TariffsSAXHandler;

public class ParseService {
	public static void parseDocument(TariffSet tariffSet, String path) throws MoblieException {
		
		try {
			// Создаём объект-парсер вида SAX
			XMLReader reader = XMLReaderFactory.createXMLReader();
			
			// Создаём объект-обработчик наших хмл-файлов
			TariffsSAXHandler handler = new TariffsSAXHandler(tariffSet);
			
			// Определяем для парсера наш хендлер в качестве обработчика содержиого 
			reader.setContentHandler(handler);
			
			// Определяем для парсера наш хендлер в качестве обработчика ошибок
			reader.setErrorHandler(handler);
			
			// Запускаем парсинг нужного хмл-файла
			reader.parse(new InputSource(path));
		} catch (SAXException ex) {
			throw new MoblieException("No default XMLReader class can be identified and instantiated!");
		} catch (IOException ex) {
			throw new MoblieException("IO exception from the parser, possibly from a byte stream or character stream supplied by the application!");
		}
	}
}
