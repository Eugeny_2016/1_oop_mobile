package by.epam.mobile.service;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.exception.MoblieException;

public class CalculationService {
	
	public static long sumConnectionFee(TariffSet tariffSet) throws MoblieException {
		long sum = 0L;
		for (Tariff tariff : tariffSet.getTariffSet()) {
			sum = sum + tariff.getConnectionFee();
		}
		
		// если из-за неправильных переданных в метод данных сумма оказалась отрицательной сообщаем об этом верхнему уровню 
		if (sum >= 0) {
			return sum;
		} else {
			throw new MoblieException("Sum is negative!");
		}		
	}	
}
