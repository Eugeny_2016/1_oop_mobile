package by.epam.mobile.service;

import java.util.Map;

import by.epam.mobile.entity.CallParameterName;
import by.epam.mobile.entity.InternetParameterName;
import by.epam.mobile.entity.TariffName;

public class ValidationService {
		
	// проверка полей тарифов для звонков
	public static boolean validate(int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		boolean result = validateCommonPart(tariffId, operatorName, tariffName, connectionFee, 
											initialPayment, internetSpeedDefault, smsPrice, mmsPrice,  
											nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		
		if (result) {
			if (everyMegabytePrice < 0) {
				return false;
			}
		} else {
			return false;
		}
				
		return true;
	}
	
	// проверка полей тарифов для интернета
	public static boolean validate(int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
									   int internetSpeedDefault, int internetSpeedMax, int smsPrice, int mmsPrice,
									   Map<CallParameterName, Object> nativePriceTable, 
									   Map<CallParameterName, Object> stationaryPriceTable, 
									   Map<CallParameterName, Object> otherNetworkPriceTable, 
									   Map<InternetParameterName, Object> internetPriceTable) {
		
		boolean result = validateCommonPart(tariffId, operatorName, tariffName, connectionFee, 
				initialPayment, internetSpeedDefault, smsPrice, mmsPrice,  
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		
		if (result) {
			if (internetPriceTable.get(InternetParameterName.MEGABYTES_WITH_DISCOUNT) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.MEGABYTES_WITH_DISCOUNT) < 0) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.DISCOUNT_MEGABYTE_PRICE) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.DISCOUNT_MEGABYTE_PRICE) < 0) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.COMMON_MEGABYTE_PRICE) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.COMMON_MEGABYTE_PRICE) < 0) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.AMOUNT_OF_TRAFFIC) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.AMOUNT_OF_TRAFFIC) < 0) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC) < 0) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.valueOf(0));
			}
						
			if (internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE) != null) {
				if (!"day".equals(internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE)) && 
					!"month".equals(internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE))) {
					return false;
				}
			} else {
				internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, "month");
			}
			
		} else { 
			return false;
		}
			
		return true;
	}
	
	// проверка полей общих для всех тарифов
	public static boolean validateCommonPart(int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
							  int internetSpeedDefault, int smsPrice, int mmsPrice, 
							  Map<CallParameterName, Object> nativePriceTable, 
							  Map<CallParameterName, Object> stationaryPriceTable, 
							  Map<CallParameterName, Object> otherNetworkPriceTable) {
		boolean result = true;

		if (tariffId < 0) {
			result = false;
		}
		
		if (!"MTS".equals(operatorName)) {
			result = false;
		}
		
		if (tariffName == null) {
			return false;
		}
		
		if (connectionFee < 0) {
			result = false;
		}
		
		if (initialPayment < 0) {
			result = false;
		}
		
		if (internetSpeedDefault < 0) {
			result = false;
		}
		
		if (smsPrice < 0) {
			result = false;
		}
		
		if (mmsPrice < 0) {
			result = false;
		}
		
		if (!validateMapFields(nativePriceTable)) {
			result = false;
		}
		
		if (!validateMapFields(stationaryPriceTable)) {
			result = false;
		}
		
		if (!validateMapFields(otherNetworkPriceTable)) {
			result = false;
		}
		
		return result;
	}

	// проверка полей типа Map для звонков
	public static boolean validateMapFields(Map<CallParameterName, Object> table) {
		boolean result = true; 
		
		if (table.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT) != null) {
			if ((Integer)table.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT) < 0) {
				result = false;
			}
		} else {
			table.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.MINUTES_WITH_DISCOUNT) != null) {
			if ((Integer)table.get(CallParameterName.MINUTES_WITH_DISCOUNT) < 0) {
				result = false;
			}
		} else {
			table.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.DISCOUNT_MINUTE_PRICE) != null) {
			if ((Integer)table.get(CallParameterName.DISCOUNT_MINUTE_PRICE) < 0) {
				result = false;
			}
		} else {
			table.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.COMMON_MINUTE_PRICE) != null) {
			if ((Integer)table.get(CallParameterName.COMMON_MINUTE_PRICE) < 0) {
				result = false;
			}
		} else {
			table.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.MINUTE_TYPE) != null) {
			if (!"call".equals(table.get(CallParameterName.MINUTE_TYPE)) &&
				!"day".equals(table.get(CallParameterName.MINUTE_TYPE)) &&
				!"month".equals(table.get(CallParameterName.MINUTE_TYPE))) {
				result = false;
			}
		} else {
			table.put(CallParameterName.MINUTE_TYPE, "call");
		}
		
		return result;
	}

}
