package by.epam.mobile.service;

import java.util.Set;
import java.util.TreeSet;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.service.comparator.TariffComparator;

public class SortService {
	
	public static Set<Tariff> sortByComparator(TariffSet tariffSet, TariffComparator tariffComparator) {
		Set<Tariff> sortedTariffSet = new TreeSet<Tariff>(tariffComparator);
		sortedTariffSet.addAll(tariffSet.getTariffSet());
		
		return sortedTariffSet;			
	}

}
