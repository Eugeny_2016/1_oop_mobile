package by.epam.mobile.service;

//перечисление типов звонков, которые используются при сортировке и поиске
public enum CallType {
	NATIVE, STATIONARY, OTHERNET;
}
