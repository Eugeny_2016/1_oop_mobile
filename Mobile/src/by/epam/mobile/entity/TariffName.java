package by.epam.mobile.entity;

public enum TariffName {
	OTLICHNIY,
	OBSHAISYA, 
	LEGKO_SKAZAT, 
	SMART, 
	SMART_MINI, 
	SMART_PLUS, 
	INTERNET_MINI, 
	INTERNET_MIDI, 
	INTERNET_MAXI, 
	BIT, 
	SUPER_BIT, 
	VIP_BIT;
}
