package by.epam.mobile.entity;

import java.util.Collections;
import java.util.Map;

public class Tariff {
	
	private int tariffId;
	private String operatorName;
	private TariffName tariffName;
	private int connectionFee;
	private int initialPayment;
	private int internetSpeedDefault;
	private int smsPrice;
	private int mmsPrice;
	private Map<CallParameterName, Object> nativePriceTable;
	private Map<CallParameterName, Object> stationaryPriceTable;
	private Map<CallParameterName, Object> otherNetworkPriceTable;
		
	public Tariff(int tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, 
			Map<CallParameterName, Object> nativePriceTable, Map<CallParameterName, Object> stationaryPriceTable, Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		this.tariffId = tariffId;
		this.operatorName = operatorName;
		this.tariffName = tariffName;
		this.connectionFee = connectionFee;
		this.initialPayment = initialPayment;
		this.internetSpeedDefault = internetSpeedDefault;
		this.smsPrice = smsPrice;
		this.mmsPrice = mmsPrice;
		this.nativePriceTable = nativePriceTable;
		this.stationaryPriceTable = stationaryPriceTable;
		this.otherNetworkPriceTable = otherNetworkPriceTable;
	}
		
	public int getId() {
		return tariffId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public TariffName getTariffName() {
		return tariffName;
	}

	public int getConnectionFee() {
		return connectionFee;
	}
	
	public int getInitialPayment() {
		return initialPayment;
	}
	
	public int getInternetSpeedDefault() {
		return internetSpeedDefault;
	}
	
	public int getSmsPrice() {
		return smsPrice;
	}

	public int getMmsPrice() {
		return mmsPrice;
	}
	
	public Map<CallParameterName, Object> getNativePriceTable() {
		return Collections.unmodifiableMap(nativePriceTable);
	}

	public Map<CallParameterName, Object> getStationaryPriceTable() {
		return Collections.unmodifiableMap(stationaryPriceTable);
	}

	public Map<CallParameterName, Object> getOtherNetworkPriceTable() {
		return Collections.unmodifiableMap(otherNetworkPriceTable);
	}

	public void setId(int id) {
		this.tariffId = id;
	}
	
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public void setTariffName(TariffName tariffName) {
		this.tariffName = tariffName;
	}

	public void setConnectionFee(int connectionFee) {
		this.connectionFee = connectionFee;
	}
	
	public void setInitialPayment(int initialPayment) {
		this.initialPayment = initialPayment;
	}
	
	public void setInternetSpeedDefault(int internetSpeedDefault) {
		this.internetSpeedDefault = internetSpeedDefault;
	}

	public void setSmsPrice(int smsPrice) {
		this.smsPrice = smsPrice;
	}

	public void setMmsPrice(int mmsPrice) {
		this.mmsPrice = mmsPrice;
	}
	
	public void setNativePriceTable(Map<CallParameterName, Object> nativePriceTable) {
		this.nativePriceTable = nativePriceTable;
	}

	public void setStationaryPriceTable(Map<CallParameterName, Object> stationaryPriceTable) {
		this.stationaryPriceTable = stationaryPriceTable;
	}

	public void setOtherNetworkPriceTable(Map<CallParameterName, Object> otherNetworkPriceTable) {
		this.otherNetworkPriceTable = otherNetworkPriceTable;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Tariff's id: ").append(tariffId);
		str.append(" Operator name: ").append(operatorName);
		str.append(" Tariff name: ").append(tariffName);
		str.append(" Connection fee: ").append(connectionFee);
		str.append(" Initial payment: ").append(initialPayment);
		str.append(" Internet default speed: ").append(internetSpeedDefault);
		str.append(" SMS price: ").append(smsPrice);
		str.append(" MMS price: ").append(mmsPrice);
		str.append(" Native network call prices: ").append(nativePriceTable);
		str.append(" Stationary network call prices: ").append(stationaryPriceTable);
		str.append(" Other networks call prices: ").append(otherNetworkPriceTable);
		return str.toString();
	}

	@Override
	public int hashCode() {
		return (int) (tariffId*31 + operatorName.hashCode()*31 + tariffName.hashCode()*25 + connectionFee*9
				+ initialPayment*18 + internetSpeedDefault*11 + smsPrice*5 + mmsPrice*31 + nativePriceTable.hashCode()*10
				+ stationaryPriceTable.hashCode()*24 + otherNetworkPriceTable.hashCode()*7);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Tariff tariff = (Tariff) obj;
		if (tariffId != tariff.tariffId) {
			return false;
		}
		
		if (!operatorName.equals(tariff.operatorName)) {
			return false;
		}
		
		if (!tariffName.equals(tariff.tariffName)) {
			return false;
		}
		
		if (connectionFee != tariff.connectionFee) {
			return false;
		}
		
		if (initialPayment != tariff.initialPayment) {
			return false;
		}
		
		if (internetSpeedDefault != tariff.internetSpeedDefault) {
			return false;
		}
		
		if (smsPrice != tariff.smsPrice) {
			return false;
		}
		
		if (mmsPrice != tariff.mmsPrice) {
			return false;
		}
		
		if (!this.nativePriceTable.equals(tariff.nativePriceTable)) {
			return false;
		}
		
		if (!this.stationaryPriceTable.equals(tariff.stationaryPriceTable)) {
			return false;
		}
		
		if (!this.otherNetworkPriceTable.equals(tariff.otherNetworkPriceTable)) {
			return false;
		}
		
		return true;
	}
	
}
