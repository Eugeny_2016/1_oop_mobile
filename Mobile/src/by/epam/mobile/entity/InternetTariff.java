package by.epam.mobile.entity;

import java.util.Collections;
import java.util.Map;

public class InternetTariff extends Tariff {
	
	private Map<InternetParameterName, Object> internetPriceTable;
	private int internetSpeedMax;
	
	public InternetTariff(int tariffId, String operatorName, TariffName tariffName, int connectionFee,
			int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, int maxInternetSpeed, 
			Map<CallParameterName, Object> nativePriceTable,
			Map<CallParameterName, Object> stationaryPriceTable,
			Map<CallParameterName, Object> otherNetworkPriceTable,
			Map<InternetParameterName, Object> internetPriceTable) {
		
		super(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, smsPrice, mmsPrice,
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		
		this.internetPriceTable = internetPriceTable;
		this.internetSpeedMax = maxInternetSpeed;
		
	}
	
	public Map<InternetParameterName, Object> getInternetPriceTable() {
		return Collections.unmodifiableMap(internetPriceTable);
	}

	public int getMaxInternetSpeed() {
		return internetSpeedMax;
	}

	public void setInternetPriceTable(Map<InternetParameterName, Object> internetPriceTable) {
		this.internetPriceTable = internetPriceTable;
	}
	
	public void setMaxInternetSpeed(int internetSpeedMax) {
		this.internetSpeedMax = internetSpeedMax;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(super.toString());
		str.append(" Internet prices: ").append(internetPriceTable);
		str.append(" Internet maximum speed: ").append(internetSpeedMax);
		
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return (int) (super.hashCode() + internetPriceTable.hashCode()*7 + internetSpeedMax*10);
	}

	@Override
	public boolean equals(Object obj) {
		InternetTariff internetTariff = (InternetTariff) obj;
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!this.internetPriceTable.equals(internetTariff.internetPriceTable)) {
			return false;
		}
		
		if (internetSpeedMax != internetTariff.internetSpeedMax) {
			return false;
		}
		
		return true;
	}

}
