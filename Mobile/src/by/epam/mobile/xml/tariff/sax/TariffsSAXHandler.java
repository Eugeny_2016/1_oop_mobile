package by.epam.mobile.xml.tariff.sax;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.mobile.entity.CallParameterName;
import by.epam.mobile.entity.InternetParameterName;
import by.epam.mobile.entity.TariffName;
import by.epam.mobile.entity.TariffSet;
import by.epam.mobile.service.AddService;
import by.epam.mobile.service.ValidationService;

public class TariffsSAXHandler extends DefaultHandler {
	
	private TariffSet tariffSet;
	
	private static final Logger LOG = Logger.getLogger(TariffsSAXHandler.class);
	
	// поля для внутренних промежуточных вычислений
	private StringBuilder text;
	private boolean nativeFlag;
	private boolean stationaryFlag;
	private boolean othernetFlag;
	private String wrongTariffName;
	
	// список полей из классов, которые будут принимать в себя значения в процессе парсинга 
	private int tariffId;				// Tariff
	private String operatorName;
	private TariffName tariffName;
	private int connectionFee;
	private int initialPayment;
	private int internetSpeedDefault;
	private int smsPrice;
	private int mmsPrice;
	private Map<CallParameterName, Object> nativePriceTable;
	private Map<CallParameterName, Object> stationaryPriceTable;
	private Map<CallParameterName, Object> otherNetworkPriceTable;
	private int everyMegabytePrice;		// CallTariff
	private Map<InternetParameterName, Object> internetPriceTable; 	// InternetTariff
	private int internetSpeedMax;
		
	public TariffsSAXHandler(TariffSet tariffSet) {
		this.tariffSet = tariffSet;
		this.nativePriceTable = new HashMap<CallParameterName, Object>();
		this.stationaryPriceTable = new HashMap<CallParameterName, Object>();
		this.otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		this.internetPriceTable = new HashMap<InternetParameterName, Object>();
	}
	
	@Override
	public void startDocument() {
		LOG.info("Parsing process has started!\n");
	}
	
	public void endDocument() {
		LOG.info("Parsing process has come to an end!\n");
	}
	
	public void startElement(String uri, String localName, String qName, 
			Attributes attributes) {
		
		LOG.debug("start element -> " + "uri: " + uri + ", localNmae: " + localName
				+ ", qName: " + qName + "\n");
		text = new StringBuilder();
		
		switch (qName) {
		case "tariff":
			try {
				this.tariffId = Integer.parseInt(attributes.getValue("id"));
			} catch (NumberFormatException ex) {
				this.tariffId = -1; 	// если в хмл не заполнен id, то присваиваем ему -1, чтобы потом валидатор не пропустил такой тариф
			}
			break;
		case "native-network":
			this.nativeFlag = true;
			break;		
		case "stationary-network":
			this.stationaryFlag = true;
			break;
		case "other-network":
			this.othernetFlag = true;
			break;
		}
	}
	
	public void characters(char[] buffer, int start, int length) {
		text.append(buffer, start, length);
	}
	
	public void endElement(String uri, String localNmae, String qName) {
		boolean validResult = false;
		boolean insertResult = false;
		TariffsTagName tagName = TariffsTagName.valueOf(qName.toUpperCase().replace("-", "_"));
		
		switch(tagName) {
		case OPERATOR_NAME:
			this.operatorName = text.toString();
			break;
		case TARIFF_NAME:
			try {
				this.tariffName = TariffName.valueOf(text.toString().toUpperCase().replace(' ', '_'));
			} catch (IllegalArgumentException ex) {
				this.tariffName = null;
				this.wrongTariffName = text.toString();
			}
			break;
		case CONNECTION_FEE:
			this.connectionFee = Integer.parseInt(text.toString());
			break;
		case INITIAL_PAYMENT:
			this.initialPayment = Integer.parseInt(text.toString());
			break;
		case INTERNET_SPEED_DAFAULT:
			this.internetSpeedDefault = Integer.parseInt(text.toString());
			break;
		case INTERNET_SPEED_MAX:
			this.internetSpeedMax = Integer.parseInt(text.toString());
			break;
		case SMS:
			this.smsPrice = Integer.parseInt(text.toString());
			break;
		case MMS:
			this.mmsPrice = Integer.parseInt(text.toString());
			break;
		case MINUTES_WITHOUT_DISCOUNT:
			if (nativeFlag) {
				this.nativePriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				this.stationaryPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				this.otherNetworkPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			}
			break;
		case MINUTES_WITH_DISCOUNT:
			if (nativeFlag) {
				this.nativePriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				this.stationaryPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				this.otherNetworkPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			}
			break;
		case DISCOUNT_MINUTE_PRICE:
			if (nativeFlag) {
				this.nativePriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				this.stationaryPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				this.otherNetworkPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			}
			break;
		case COMMON_MINUTE_PRICE:
			if (nativeFlag) {
				this.nativePriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				this.stationaryPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				this.otherNetworkPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			}
			break;
		case MINUTE_TYPE:
			if (nativeFlag) {
				this.nativePriceTable.put(CallParameterName.MINUTE_TYPE,text.toString());
			} else if (stationaryFlag) {
				this.stationaryPriceTable.put(CallParameterName.MINUTE_TYPE, text.toString());
			} else if (othernetFlag) {
				this.otherNetworkPriceTable.put(CallParameterName.MINUTE_TYPE, text.toString());
			}
			break;
		case MEGABYTES_WITH_DISCOUNT:
			this.internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			break;
		case DISCOUNT_MEGABYTE_PRICE:
			this.internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(text.toString()));
			break;
		case COMMON_MEGABYTE_PRICE:
			this.internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(text.toString()));
			break;
		case AMOUNT_OF_TRAFFIC:
			this.internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(text.toString()));
			break;
		case AMOUNT_OF_FAST_TRAFFIC:
			this.internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(text.toString()));
			break;
		case MEGABYTE_TYPE:
			this.internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, text.toString());
			break;
		case EVERY_MEGABYTE_PRICE:
			this.everyMegabytePrice = Integer.parseInt(text.toString());
			break;
		case NATIVE_NETWORK:
			this.nativeFlag = false;
			break;
		case STATIONARY_NETWORK:
			this.stationaryFlag = false;
			break;
		case OTHER_NETWORK:
			this.othernetFlag = false;
			break;
		case TARIFF:
			if (tariffName == null) {
				LOG.info("Can't add tariff \"" + wrongTariffName + "\"\n");
				LOG.info("Tariff " + wrongTariffName + " denied!\n");
				break;
			}
			switch (tariffName) {
			case OTLICHNIY:
			case OBSHAISYA:
			case LEGKO_SKAZAT:
				// отправляем данные на валидацию
				validResult = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, 
														smsPrice, mmsPrice, everyMegabytePrice, 
														nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
				// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
				if (validResult) {
					insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, 
														smsPrice, mmsPrice, everyMegabytePrice, 
														nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
					LOG.info("Validation result is true!\n");
					if (insertResult) {
						LOG.info("Insertion result is true!\n");
						LOG.info("Tariff " + tariffName + " accepted!\n");
					} else {
						LOG.info("Insertion result is false!\n");
						LOG.info("Tariff " + tariffName + " denied!\n");
					}
					
				} else {
					LOG.info("Validation result is false!\n");
					LOG.info("Tariff " + tariffName + " denied!\n");
				}
				break;
			case SMART:
			case SMART_MINI:
			case SMART_PLUS:
			case INTERNET_MINI:
			case INTERNET_MIDI:
			case INTERNET_MAXI:
			case BIT:
			case SUPER_BIT:
			case VIP_BIT:
				// отправляем данные на валидацию
				validResult = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														internetSpeedDefault, internetSpeedMax, smsPrice, mmsPrice, 
														nativePriceTable, stationaryPriceTable, otherNetworkPriceTable,
														internetPriceTable);
				// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
				if (validResult) {
					insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														internetSpeedDefault, internetSpeedMax, smsPrice, mmsPrice, 
														nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, 
														internetPriceTable);
					LOG.info("Validation result is true!\n");
					if (insertResult) {
						LOG.info("Insertion result is true!\n");
						LOG.info("Tariff " + tariffName + " accepted!\n");
					} else {
						LOG.info("Insertion result is false!\n");
						LOG.info("Tariff " + tariffName + " denied!\n");
					}
					
				} else {
					LOG.info("Validation result is false!\n");
					LOG.info("Tariff " + tariffName + " denied!\n");
				}
				break;
			}
			break;
		}
	}
	
	public void warning(SAXParseException exception) {
		System.out.print("WARNING: line " + exception.getLineNumber() + ": "
				+ exception.getMessage() + "\n");
	}
	
	public void error(SAXParseException exception) {
		System.out.print("ERROR: line " + exception.getLineNumber() + ": "
				+ exception.getMessage());
	}
	
	public void fatalError(SAXParseException exception) throws SAXParseException {
		System.out.print("FATAL: line " + exception.getLineNumber() + ": "
				+ exception.getMessage());
		throw(exception);
	}

}
