package by.epam.mobile.xml.tariff.validation;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class TariffXSDValidation {

	public static void main(String[] args) throws SAXException, IOException {
		
		// Создаём реальную объект-фабрику для создания объектов-схем
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		// Создаём объект-файл, ссылающийся на XSD-файл по определённому пути
		File schemaLocation = new File("xml/tariffs.xsd");
		
		// Создаём объект-схему, ссылающуюся на файл с XSD-схемой
		// Может выбросить SAXException
		Schema schema = factory.newSchema(schemaLocation);
		
		// Создаём объект-валидатор, который будет руководствоваться созданной XSD-схемой
		Validator validator = schema.newValidator();
		
		// Создаём объект-источник потока данных для валидации (т.е. то, что будем валидировать)
		Source source = new StreamSource("xml/tariffs.xml");

		// Начинаем валидацию xml-файла
		try {
			validator.validate(source);
			System.out.print("Document is valid!\n");
		} catch (SAXException ex) {
			System.out.print("Document is not valid because:\n");
			System.out.print(ex.getMessage() + "\n");
		}
	}

}
