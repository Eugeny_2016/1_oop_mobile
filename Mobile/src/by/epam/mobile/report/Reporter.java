package by.epam.mobile.report;

import java.util.Set;

import by.epam.mobile.entity.Tariff;
import by.epam.mobile.service.SearchSortParameter;

public class Reporter {
	
	// метод вывода на консоль текста о наборе тарифов
	public static void displayTextTariffSet() {
		System.out.print("Your set of tariffs:\n");
	}
	
	// метод вывода на консоль текста об отсортированном наборе тарифов
	public static void displayTextAfterSorting(SearchSortParameter parameter) {
		System.out.print("Your set of tariffs after sorting by " + parameter + ":\n");
	}
	
	// метод вывода на консоль текста о сумме абонентских плат всех тарифов
	public static void displayTextAfterSumming() {
		System.out.print("The connection fee sum of all tariffs is:\n");
	}
	
	// метод вывода на консоль текста о найденных тарифах с абон. платой в диапазоне
	public static void displayTextAfterPositiveSearching(int searchRangeFloor, int searchRangeCell) {
		System.out.print("Tariffs that match the connection fee range [" + searchRangeFloor + "-" + searchRangeCell + "] are: \n");
	}
	
	// метод вывода на консоль текста о том, что тарифы с абон. платой в диапазоне не найдены
	public static void displayTextAfterNegativeSearching(int searchRangeFloor, int searchRangeCell) {
		System.out.print("There are no tariffs that match the connection fee range [" + searchRangeFloor + "-" + searchRangeCell + "]...\n");
	}
	
	// метод вывода на консоль числового значения
	public static void displayNumericParameter(Number parameter) {
		System.out.print(parameter + "\n");
	}
	
	// метод для вывода на консоль набора тарифов
	public static void displayTariffSet(Set<Tariff> tariffSet) {
		for (Tariff tariff : tariffSet) {
			System.out.print(tariff + "\n");
		}
	}

}
